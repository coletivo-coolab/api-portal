# API - Portal

This is un api to use with local servers with ssb protocol

## Usage

`npm i` && `npm start`. Will start server on post 3000

Create an entry on map:

```
curl -H "Content-Type: application/json" \
    -X POST \
    --data '{"lat":"-47","lon":"-14"}' \
    http://localhost:3000/map | json_pp -json_opt pretty,canonical
```

Check map entries:

`curl http://localhost:3000/map | json_pp -json_opt pretty,canonical`