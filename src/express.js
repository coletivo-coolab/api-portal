const ssb = require("./ssb");
const express = require("express");
const ssbKeys = require("ssb-keys");
const app = express();
const port = 3000;
const { where, and, type, author, toCallback } = require("ssb-db2/operators");

app.use(express.json());

/*
    publishData
    key: {
        private
        curve
        id
        public
    }
    private: Boolean
    content: {
        type: String,
        ...
    },
*/

const keys = ssbKeys.generate();

app.post("/map", async (req, res) => {
  if (!req.body) throw "No body";
  const { lat, lon } = req.body;
  if (!lat || !lon) throw "Format: {lat: Int, lon: Int}";
  const content = {
    type: "map",
    lat,
    lon,
  };
  ssb.db.publishAs(keys, content, (err, data) => {
    if (err) {
      console.log('ERROR:', err)
      res.json(err)
    };
    console.log("DATA", data);
    res.json(data);
  });
});

app.get("/map", async (req, res) => {
  ssb.db.query(
    where(and(type("map"))),
    toCallback((err, msgs) => {
      if (err) res.json(err);
      console.log("There are " + msgs.length + " messages of map");
      const formatedRespose = msgs.map((i) => {
        const { author, content, timestamp } = i.value;
        return {
          key: i.key,
          author,
          content,
          timestamp,
        };
      });
      res.json(formatedRespose);
    })
  );
});

app.get("/about", async (req, res) => {
  const keys = ssbKeys.generate();
  ssb.db.onDrain("aboutSelf", () => {
    const profile = ssb.db.getIndex("aboutSelf").getProfile(keys.id);
    console.log("Alice has name:" + profile.name);
  });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

module.exports = app;
