const fs = require("fs");
const path = require("path");
const opts = {}
let config = require("ssb-config/inject")('api-portal',opts)

const ssbFolder = () => {
  let homeFolder =
    process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;
  return `${homeFolder}/.${process.env.CONFIG_FOLDER || 'api-portal'}`;
};

const writeKey = (key, path) => {
  let secretPath = `${ssbFolder()}${path}`;

  // Same options ssb-keys use
  try {
    fs.mkdirSync(ssbFolder(), { recursive: true });
  } catch (e) {}
  fs.writeFileSync(secretPath, key, { mode: 0x100, flag: 'wx' });
};

const envKey =
  process.env.SSB_KEY &&
  Buffer.from(process.env.SSB_KEY, "base64").toString("utf8");
const secretExists = fs.existsSync(`${ssbFolder()}/secret`);

if (!secretExists && envKey) {
  writeKey(envKey, "/secret");
  console.log("Writing SSB_KEY from env");
  if (!fs.existsSync(`${ssbFolder()}/gossip.json`)) {
    fs.copyFileSync("gossip.json", `${ssbFolder()}/gossip.json`);
  }
}
// Need to use secret-stack directly instead of ssb-server here otherwise is not compatible with patchwork .ssb folder
const Server = require("secret-stack")()
  .use(require('ssb-master'))
  .use(require("ssb-db2"))
  .use(require('ssb-db2/compat')) // include all compatibility plugins
  .use(require('ssb-db2/full-mentions')) // include index
  .use(require('ssb-db2/about-self')) // include index
  .use(require('ssb-ebt'))
  .use(require('ssb-friends'))
  .use(require('ssb-replication-scheduler'))
  .use(require('ssb-conn'))
  .use(require('ssb-room-client'))
  .use(require("ssb-invite"))
  .use(require('ssb-blobs'))
  // .use(require('ssb-backlinks'))
  .use(require('ssb-serve-blobs'))
  // .use(require('./plugins/feedless-index'))

const server = Server(config);
console.log("SSB server started at", config.port);
console.log('Starting SSB:', server.whoami().id)

// save an updated list of methods this server has made public
// in a location that ssb-client will know to check
const manifest = server.getManifest();
fs.writeFileSync(
  path.join(config.path, "manifest.json"), // ~/.ssb/manifest.json
  JSON.stringify(manifest)
);

// SSB server automatically creates a secret key, but we want the user flow where they choose to create a key or use an existing one
/* const mode = process.env.MODE || "standalone";
if (mode == "standalone" && !secretExists) {
  fs.writeFileSync(`${ssbFolder()}/logged-out`, "");
} */

module.exports = server